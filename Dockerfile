FROM nginx:stable-alpine

RUN rm /etc/nginx/conf.d/*

ADD app-nginx-example.conf /etc/nginx/conf.d/
